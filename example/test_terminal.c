#include <glib-object.h>
#include <gtk/gtk.h>
#include <tiled.h>
#include <vte/vte.h>

GtkApplication *application;
GtkWidget *window;
TiledContainer *main_container;

void
ended_child (VteTerminal *terminal,
             gint         error_value,
             gpointer     data)
{
    GtkWidget *tiled_element = data;
    tiled_element_do_close_tile (TILED_ELEMENT (tiled_element));
}

void
close_terminal (GtkButton *button,
                gpointer   data)
{
    GtkWidget *tiled_element = data;
    tiled_element_do_close_tile (TILED_ELEMENT (tiled_element));
}

void
close_window () {
    gtk_window_close (GTK_WINDOW (window));
}

void
split_horizontal (GtkButton *button,
                  gpointer   data)
{
    GtkWidget *tiled_element = data;
    tiled_element_do_split_tile (TILED_ELEMENT (tiled_element), TILED_SPLIT_TYPE_BOTTOM, 0);
}

void
split_vertical (GtkButton *button,
                gpointer   data)
{
    GtkWidget *tiled_element = data;
    tiled_element_do_split_tile (TILED_ELEMENT (tiled_element), TILED_SPLIT_TYPE_RIGHT, 0);
}

gboolean
key_pressed (VteTerminal  *terminal,
             GdkEventKey  *event,
             TiledElement *element)
{
    TiledElement *neighbour;

    event->state &= GDK_SHIFT_MASK | GDK_CONTROL_MASK;

    if (event->state != (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) {
        return FALSE;
    }
    switch (event->keyval)
    {
    case GDK_KEY_Up:
        neighbour = tiled_element_get_neighbour (element, TILED_NEIGHBOUR_DIRECTION_UP);
        break;
    case GDK_KEY_Down:
        neighbour = tiled_element_get_neighbour (element, TILED_NEIGHBOUR_DIRECTION_DOWN);
        break;
    case GDK_KEY_Left:
        neighbour = tiled_element_get_neighbour (element, TILED_NEIGHBOUR_DIRECTION_LEFT);
        break;
    case GDK_KEY_Right:
        neighbour = tiled_element_get_neighbour (element, TILED_NEIGHBOUR_DIRECTION_RIGHT);
        break;
    }
    if (neighbour) {
        VteTerminal *new_terminal = g_object_get_data (G_OBJECT (neighbour), "terminal");
        gtk_widget_grab_focus (GTK_WIDGET (new_terminal));
    }
    return TRUE;
}

void do_realize (VteTerminal  *terminal,
                 TiledElement *element)
{
    gtk_widget_grab_focus (GTK_WIDGET (terminal));
}


/**
 * This function creates a new TiledElement with a terminal and split and close
 * buttons inside. Every tile will be created with this function.
 */
TiledElement *
create_tiled_element()
{
    // First, create a TiledElement
    GtkWidget *tiled_element = tiled_element_new();

    // Now create all the elements that will be inside:
    GtkWidget *close, *horizontal, *vertical, *vbox, *hbox, *hbox2, *terminal, *scroll;

    // the three buttons (for horizontal and vertical splitting, and close the tile)
    close = gtk_button_new_with_label ("X");
    horizontal = gtk_button_new_with_label ("-");
    vertical = gtk_button_new_with_label ("|");

    // The VTE terminal
    terminal = vte_terminal_new ();

    // The scroll bar
    scroll = gtk_scrollbar_new( GTK_ORIENTATION_VERTICAL, gtk_scrollable_get_vadjustment (GTK_SCROLLABLE (terminal)));

    // And the GtkBoxes to contain everything
    vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);
    hbox2 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);

    // Now pack everything in the GtkBoxes
    gtk_box_pack_start (GTK_BOX (hbox), horizontal, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (hbox), vertical, TRUE, TRUE, 2);
    gtk_box_pack_start (GTK_BOX (hbox), close, TRUE, TRUE, 2);

    gtk_box_pack_start (GTK_BOX (hbox2), terminal, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (hbox2), scroll, FALSE, FALSE, 0);

    // and put all inside the TiledElement
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox2, TRUE, TRUE, 0);

    gtk_container_add (GTK_CONTAINER (tiled_element), vbox);

    // Launch a BASH inside the terminal
    char *args[] = {"bash", NULL};
    vte_terminal_spawn_async ( VTE_TERMINAL (terminal), VTE_PTY_DEFAULT, g_get_current_dir(), args, g_get_environ(), 0, NULL, NULL, NULL, -1, NULL, NULL, NULL);

    // And finally, connect all the signals to the callbacks
    g_signal_connect (G_OBJECT (terminal), "child-exited", G_CALLBACK (ended_child), tiled_element);
    g_signal_connect (G_OBJECT (close), "clicked", G_CALLBACK (close_terminal), tiled_element);
    g_signal_connect (G_OBJECT (horizontal), "clicked", G_CALLBACK (split_horizontal), tiled_element);
    g_signal_connect (G_OBJECT (vertical), "clicked", G_CALLBACK (split_vertical), tiled_element);
    g_signal_connect (G_OBJECT (terminal), "key-press-event", G_CALLBACK (key_pressed), tiled_element);
    g_signal_connect (G_OBJECT (terminal), "realize", G_CALLBACK (do_realize), tiled_element);
    g_object_set_data (G_OBJECT (tiled_element), "terminal", terminal);

    return TILED_ELEMENT (tiled_element);
}

/**
 * This is the callback for the added-tile signal. It creates a new TiledElement
 * with a terminal and everything else inside, and inserts it in the new TiledContainer
 * that has been created.
 */
static void added_tile (TiledContainer *top_container,
                        TiledContainer *new_container,
                        guint           split_data,
                        gpointer        data)
{
    TiledElement *element = create_tiled_element();
    tiled_container_set_tiled_element(new_container, element);
}

void activate_function ()
{
    window = gtk_application_window_new (application);

    main_container = TILED_CONTAINER (tiled_container_new (create_tiled_element(), NULL));
    g_signal_connect (G_OBJECT (main_container), "empty-tree", G_CALLBACK (close_window), NULL);
    g_signal_connect (G_OBJECT (main_container), "added-tile", G_CALLBACK (added_tile), NULL);

    gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (main_container));
    gtk_widget_show_all (window);
}

int main(int argc, char **argv)
{
    application = gtk_application_new ("com.rastersoft.tiledtest", G_APPLICATION_DEFAULT_FLAGS);
    g_signal_connect (G_OBJECT (application), "activate", G_CALLBACK (activate_function), NULL);
    g_application_run (G_APPLICATION (application), argc, argv);
}
