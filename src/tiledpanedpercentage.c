/* -*- Mode: C; c-file-style: "gnu"; tab-width: 4 -*- */
/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tiledpanedpercentage.h"
#include <cairo-gobject.h>

/**
 * SECTION:tiledpanedpercentage
 * @Short_description: A GtkPaned that preserves the size relationship when resized
 * @Title: TiledPanedPercentage
 *
 * This widget is basically a GtkPaned, but it preserves the size relationship between
 * both panes when resized, instead of keeping one fixed and resizing the other. This
 * is only needed in GTK3, because the GtkPaned in GTK4 already supports this feature.
 */

typedef struct {
  gint32 current_paned_position;
  gint32 current_paned_size;
  gdouble desired_paned_percentage;
  gboolean changed_paned_size;
} TiledPanedPercentagePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (TiledPanedPercentage, tiled_paned_percentage, GTK_TYPE_PANED)

static void
size_allocate (GtkWidget *widget, GdkRectangle *allocation, gpointer user_data)
{
  GValue value = {0};
  g_value_init(&value, G_TYPE_INT);

  TiledPanedPercentage *self = TILED_PANED_PERCENTAGE (widget);
  g_object_get_property(G_OBJECT(self), "orientation", &value);
  TiledPanedPercentagePrivate *priv = tiled_paned_percentage_get_instance_private (self);
  if (g_value_get_int(&value) == GTK_ORIENTATION_VERTICAL)
    {
      if (priv->current_paned_size != allocation->height)
      {
        priv->current_paned_size = allocation->height;
          priv->changed_paned_size = TRUE;
      }
    }
  else
    {
      if (priv->current_paned_size != allocation->width)
        {
          priv->current_paned_size = allocation->width;
          priv->changed_paned_size = TRUE;
        }
    }
}

static gboolean
draw (GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
  TiledPanedPercentage *self = TILED_PANED_PERCENTAGE (widget);
  TiledPanedPercentagePrivate *priv = tiled_paned_percentage_get_instance_private (self);
  GValue value = {0};
  int ivalue;

  g_value_init(&value, G_TYPE_INT);
  if (priv->changed_paned_size)
    {
      priv->current_paned_position = (int) (priv->current_paned_size * priv->desired_paned_percentage);
      g_value_set_int(&value, priv->current_paned_position);
      g_object_set_property(G_OBJECT(self), "position", &value);
      priv->changed_paned_size = FALSE;
    }
  else
    {
      g_object_get_property(G_OBJECT(self), "position", &value);
      ivalue = g_value_get_int(&value);
      if (priv->current_paned_position != ivalue)
        {
          priv->current_paned_position = ivalue;
          priv->desired_paned_percentage = ((double) priv->current_paned_position) / ((double) priv->current_paned_size);
        }
    }
  return FALSE;
}

static void
tiled_paned_percentage_class_init (TiledPanedPercentageClass *klass)
{
}

static void
tiled_paned_percentage_init (TiledPanedPercentage *self)
{
  TiledPanedPercentagePrivate *priv = tiled_paned_percentage_get_instance_private (self);

  priv->current_paned_position = -1;
  priv->current_paned_size = -1;
  priv->changed_paned_size = FALSE;
  priv->desired_paned_percentage = 0.5;
  g_signal_connect_after(self, "size-allocate", G_CALLBACK (size_allocate), NULL);
  g_signal_connect(self, "draw", G_CALLBACK (draw), NULL);
}

/**
 * tiled_paned_percentage_new:
 * @orientation: the paned’s orientation.
 *
 * Creates a new #TiledPanedPercentage widget.
 *
 * Returns: a new #TiledPanedPercentage.
 *
 */
GtkWidget *
tiled_paned_percentage_new (GtkOrientation orientation)
{
  return GTK_WIDGET (g_object_new (TILED_TYPE_PANED_PERCENTAGE,
                                   "orientation", orientation,
                                   NULL));
}
