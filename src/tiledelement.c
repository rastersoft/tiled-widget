/* -*- Mode: C; c-file-style: "gnu"; tab-width: 4 -*- */
/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tiledelement.h"
#include "tiledcontainer.h"

/**
 * SECTION:tiledelement
 * @Short_description: The container of each tile
 * @Title: TiledElement
 *
 * This class is the base for each tile. Whenever a tile is splitted, a new
 * @TiledElement must be created and populated with the widgets desired for that
 * tile. It is this widget the one that has the interface for all the operations,
 * like splitting or closing it.
 *
 * This is a derivable type, which allows to subclass it to generate all the
 * inners.
 */

typedef struct _TiledElementPrivate TiledElementPrivate;

struct _TiledElementPrivate {
  TiledContainer *parent_container;
};

enum {
  CLOSE,
  REQUEST_FOCUS,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE_WITH_PRIVATE (TiledElement, tiled_element, GTK_TYPE_BIN)


static void
tiled_element_dispose (GObject *object)
{
  TiledElementPrivate *priv = tiled_element_get_instance_private (TILED_ELEMENT (object));

  g_clear_object (&priv->parent_container);

  G_OBJECT_CLASS (tiled_element_parent_class)->dispose (object);
}


static void
tiled_element_class_init (TiledElementClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->dispose = tiled_element_dispose;

  signals[CLOSE] = g_signal_new ("close-tile",
    G_OBJECT_CLASS_TYPE (gobject_class),
    G_SIGNAL_RUN_LAST,
    0,
    NULL,
    NULL,
    NULL,
    G_TYPE_NONE,
    0);
  signals[REQUEST_FOCUS] = g_signal_new ("request-focus",
    G_OBJECT_CLASS_TYPE (gobject_class),
    G_SIGNAL_RUN_LAST,
    0,
    NULL,
    NULL,
    NULL,
    G_TYPE_NONE,
    0);
}

static void
tiled_element_init (TiledElement *self)
{
  TiledElementPrivate *priv = tiled_element_get_instance_private (TILED_ELEMENT (self));

  priv->parent_container = NULL;
}

/**
 * tiled_element_split:
 * @element: a @TiledElement
 * @type: the split type desired
 * @split_data: custom data for the split.
 *
 * The children inside this @TiledElement must call this method to add a new tile
 * to its right or to its bottom. Internally it will call the parent @TiledContainer
 * and it will do all the job.
 */
void
tiled_element_do_split_tile (TiledElement    *element,
                     TiledSplitType  type,
                     guint32         split_data)
{
  TiledElementPrivate *priv;

  g_return_if_fail (TILED_IS_ELEMENT (element));

  priv = tiled_element_get_instance_private (TILED_ELEMENT (element));
  tiled_container_split (priv->parent_container, type, split_data);
}

/**
 * tiled_element_get_neighbour:
 * @element: a @TiledElement
 * @direction: the direction into which search for a neighbour
 *
 * This method will return the @TiledElement located in the direction specified in @direction.
 * It is useful to move the focus from one tile to any of its neighbours using, for example,
 * the cursors.
 *
 * returns: (nullable) (transfer none): the neighbour located in the specified direction, or NULL
 * if there are no neighbours.
 */

TiledElement *
tiled_element_get_neighbour (TiledElement            *element,
                             TiledNeighbourDirection  direction)
{
  TiledElementPrivate *priv;

  g_return_val_if_fail (TILED_IS_ELEMENT (element), NULL);

  priv = tiled_element_get_instance_private (TILED_ELEMENT (element));
  return tiled_container_get_neighbour (priv->parent_container, direction);
}

/**
 * tiled_element_close:
 * @element: a @TiledElement
 *
 * Calling this method will remove this @TiledElement from the tiles tree.
 */
void
tiled_element_do_close_tile (TiledElement *element)
{
  g_return_if_fail (TILED_IS_ELEMENT (element));

  g_signal_emit (element,
                 signals[CLOSE],
                 0);
}

/**
 * tiled_element_update_parent:
 * @element: a @TiledElement
 * @parent: a @GtkWidget
 *
 * This method is used internally by the parent @TiledContainer, and should not
 * be used externally.
 */
void
tiled_element_update_parent (TiledElement *element,
                             GtkWidget    *parent)
{
  TiledElementPrivate *priv;

  g_return_if_fail (TILED_IS_ELEMENT (element));
  g_return_if_fail ((parent == NULL) || TILED_IS_CONTAINER (parent));

  priv = tiled_element_get_instance_private (TILED_ELEMENT (element));
  g_clear_object (&priv->parent_container);
  if (parent)
    priv->parent_container = g_object_ref (TILED_CONTAINER (parent));
}

/**
 * tiled_element_do_grab_focus:
 * @element: a @TiledElement
 *
 * Used by a @TiledContainer.
 */
void
tiled_element_do_grab_focus (TiledElement *element)
{
  g_return_if_fail (TILED_IS_ELEMENT (element));
  g_signal_emit (element,
                 signals[REQUEST_FOCUS],
                 0);
}


/**
 * tiled_element_new:
 *
 * Creates a new #TiledElement widget.
 *
 * Returns: a new #TiledElement
 *
 */
GtkWidget *
tiled_element_new ()
{
  TiledElement *object = g_object_new (TILED_TYPE_ELEMENT,
                                       NULL);

  return GTK_WIDGET (object);
}
