/* -*- Mode: C; c-file-style: "gnu"; tab-width: 4 -*- */
/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tiledpanedpercentage.h"
#include "tiledcontainer.h"
#include "tiledelement.h"

/**
 * SECTION:tiledcontainer
 * @Short_description: The container of each tile, and root of the tile tree
 * @Title: TiledContainer
 *
 * This object is the key widget in the tile tree. It can contain one @TiledElement,
 * or one @TiledPanedPercentage with two @TiledContainer, one in each pane. It manages
 * all the operations for adding new tiles and removing and reordering them.
 *
 * The process to work with tiles is the following:
 *
 * * Create a new TiledElement and populate it with what you want to have in the first tile
 * * Create a new TiledContainer widget passing the previous TiledElement in the constructor,
 *   and NULL as the top_parent parameter.
 * * Connect the 'tile-added' signal of the previous TiledContainer to a function that
 *   creates a new TiledElement, populates it, and inserts it into the TiledContainer
 *   passed to the signal handler.
 *
 * The TiledElement has a method, tile_element_split(), that is used to split the TiledContainer
 * that contains that TiledElement into two. The original TiledElement will be moved into a new
 * TiledContainer, the old TiledContainer will create a TiledPanedPercentage container, insert
 * the new TiledContainer with the old TiledElement in the first place of the paned, then it
 * will create another, empty, TiledContainer and insert it in the second place of the paned, and
 * finally it will emit the 'tile-added' signal with that new, empty TiledContainer to allow the
 * main code to create a new TiledElement to populate it.
 *
 * The tile_element_split() has an extra parameter called 'split-data'. This is an integer that
 * is passed as-is to the signal handler, and can be used to allow to add different elements in
 * the new tile. It is not used by any of this code.
 *
 * Another method in TiledElement is tile_element_close(). This method destroys the TiledElement,
 * removes it from the TiledContainer tree, and reorders it conveniently. When the last TiledElement
 * is destroyed, the top TiledContainer will emit the 'empty-tree' signal to notify the main program
 * that there aren't any tiles remaining in that TiledContainer and that the window or notepad
 * containing it can be destroyed too.
 */


typedef struct _TiledContainerPrivate TiledContainerPrivate;

struct _TiledContainer
{
  GtkBin parent_instance;

  TiledContainer *container1;
  TiledContainer *container2;

  TiledPanedPercentage *paned;
  TiledElement *element;
  TiledContainer *parent_container;
  TiledContainer *top_container;

  TiledElement *last_focus_in_tree;

  gulong close_signal_id;
  gulong destroy_signal_id;

  gboolean splitted_horizontal;
};

enum {
  ADDED_TILE,
  EMPTY_TREE,
  LAST_SIGNAL
};

typedef enum {
  PROP_TILE_ELEMENT = 1,
  PROP_PARENT_CONTAINER,
  PROP_TOP_CONTAINER,
  N_PROPERTIES
} TiledContainerProperty;

static guint signals[LAST_SIGNAL] = { 0 };
static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

G_DEFINE_TYPE (TiledContainer, tiled_container, GTK_TYPE_BIN)

static TiledContainerContents *
tiled_container_contents_copy (TiledContainerContents *r)
{
  TiledContainerContents *s = g_slice_new0 (TiledContainerContents);
  s->element    = r->element ? g_object_ref (r->element) : NULL;
  s->container1 = r->container1 ? g_object_ref (r->container1) : NULL;
  s->container2 = r->container2 ? g_object_ref (r->container2) : NULL;
  s->orientation = r->orientation;
  return s;
}

static void
tiled_container_contents_free (TiledContainerContents *r)
{
  g_clear_object (&r->element);
  g_clear_object (&r->container1);
  g_clear_object (&r->container2);
  g_slice_free (TiledContainerContents, r);
}

G_DEFINE_BOXED_TYPE (TiledContainerContents, tiled_container_contents,
                     tiled_container_contents_copy,
                     tiled_container_contents_free)

static void
inner_close (TiledElement   *instance,
             TiledContainer *container)
{
  tiled_container_remove_child_element (container, instance);
}

static void
inner_remove_element (TiledContainer *container)
{
  if (container->element)
    {
      g_signal_handler_disconnect (G_OBJECT (container->element), container->close_signal_id);
      g_signal_handler_disconnect (G_OBJECT (container->element), container->destroy_signal_id);
      container->close_signal_id = 0;
      container->destroy_signal_id = 0;
      gtk_container_remove (GTK_CONTAINER (container), GTK_WIDGET (container->element));
      g_clear_object (&container->element);
    }
}

static void
tiled_container_get_property (GObject      *object,
                              guint         property_id,
                              GValue       *value,
                              GParamSpec   *pspec)
{
  TiledContainer *container = TILED_CONTAINER (object);

  switch ((TiledContainerProperty) property_id)
    {
    case PROP_TILE_ELEMENT:
      g_value_set_object (value, container->element);
      break;
    case PROP_PARENT_CONTAINER:
      g_value_set_object (value, container->parent_container);
      break;
    case PROP_TOP_CONTAINER:
      g_value_set_object (value, container->top_container);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
                                         property_id,
                                         pspec);
      break;
    }
}

static void
tiled_container_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  GObject *value_object = g_value_get_object (value);
  TiledContainer *container = TILED_CONTAINER (object);

  switch ((TiledContainerProperty) property_id)
    {
    case PROP_TILE_ELEMENT:
      tiled_container_set_tiled_element (container, value_object ? TILED_ELEMENT (value_object) : NULL);
      break;
    case PROP_PARENT_CONTAINER:
      tiled_container_set_parent_container (container, value_object ? TILED_CONTAINER (value_object) : NULL);
      break;
    case PROP_TOP_CONTAINER:
      tiled_container_set_top_container (container, value_object ? TILED_CONTAINER (value_object) : NULL);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
tiled_container_dispose (GObject *object)
{
  TiledContainer *container = TILED_CONTAINER (object);

  if (container->element)
    inner_remove_element (TILED_CONTAINER (object));

  if (container->container1)
    gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container1));

  if (container->container2)
    gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container2));

  if (container->paned)
    gtk_container_remove (GTK_CONTAINER (object), GTK_WIDGET (container->paned));

  g_clear_object (&container->container1);
  g_clear_object (&container->container2);
  g_clear_object (&container->paned);
  g_clear_object (&container->element);
  g_clear_object (&container->parent_container);
  g_clear_object (&container->top_container);
  g_clear_object (&container->last_focus_in_tree);

  G_OBJECT_CLASS (tiled_container_parent_class)->dispose (object);
}

static void
tiled_container_class_init (TiledContainerClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->get_property = tiled_container_get_property;
  gobject_class->set_property = tiled_container_set_property;
  gobject_class->dispose = tiled_container_dispose;

  signals[ADDED_TILE] = g_signal_new ("added-tile",
                                      G_OBJECT_CLASS_TYPE (gobject_class),
                                      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                                      0,
                                      NULL,
                                      NULL,
                                      NULL,
                                      G_TYPE_NONE,
                                      2,
                                      TILED_TYPE_CONTAINER,
                                      G_TYPE_UINT);

  signals[EMPTY_TREE] = g_signal_new ("empty-tree",
                                      G_OBJECT_CLASS_TYPE (gobject_class),
                                      G_SIGNAL_RUN_LAST,
                                      0,
                                      NULL,
                                      NULL,
                                      NULL,
                                      G_TYPE_NONE,
                                      0);

  obj_properties[PROP_TILE_ELEMENT] = g_param_spec_object ("tiled-element",
                                                           "Tiled element",
                                                           "The inner tiled element for this container. It must be a TiledElement or subclass.",
                                                           TILED_TYPE_ELEMENT,
                                                           G_PARAM_WRITABLE);
  obj_properties[PROP_PARENT_CONTAINER] = g_param_spec_object ("parent-container",
                                                           "Parent container",
                                                           "The TiledContainer that contains this one.",
                                                           TILED_TYPE_CONTAINER,
                                                           G_PARAM_READWRITE);

  obj_properties[PROP_TOP_CONTAINER] = g_param_spec_object ("top-container",
                                                           "Top container",
                                                           "The TiledContainer located at the top of the tree.",
                                                           TILED_TYPE_CONTAINER,
                                                           G_PARAM_READWRITE);

  g_object_class_install_properties (gobject_class, N_PROPERTIES, obj_properties);
}

static void
tiled_container_init (TiledContainer *container)
{
  container->container1 = NULL;
  container->container2 = NULL;
  container->paned = NULL;
  container->element = NULL;
  container->parent_container = NULL;
  container->top_container = NULL;
  container->last_focus_in_tree = NULL;
}

/**
 * tiled_container_split:
 * @container: (not nullable): a @TileContainer
 * @split_type: where to append the new @TileElement respect to the current one
 * @split_data: an integer that will be passed in the signal, useful to allow to choose between several @TiledElement types
 *
 * Splits the current container and adds a new element in the place specified.
 */

void
tiled_container_split (TiledContainer *container,
                       TiledSplitType  split_type,
                       guint           split_data)
{
  TiledElement *element;

  g_return_if_fail (TILED_IS_CONTAINER (container));

  if (!container->element)
    {
      if (container->container1)
        g_warning ("Trying to split an already splitted TiledContainer.");
      else
        g_warning ("Trying to split an uninitialized TiledContainer.");
      return;
    }
  container->splitted_horizontal = (split_type == TILED_SPLIT_TYPE_BOTTOM) || (split_type == TILED_SPLIT_TYPE_TOP);

  element = g_object_ref (container->element);
  tiled_container_set_tiled_element (container, NULL);
  container->paned = TILED_PANED_PERCENTAGE (g_object_ref_sink (tiled_paned_percentage_new (container->splitted_horizontal ? GTK_ORIENTATION_VERTICAL : GTK_ORIENTATION_HORIZONTAL)));
  switch (split_type)
    {
      case TILED_SPLIT_TYPE_BOTTOM:
      case TILED_SPLIT_TYPE_RIGHT:
        container->container1 = TILED_CONTAINER (tiled_container_new (element, container));
        container->container2 = TILED_CONTAINER (tiled_container_new (NULL, container));
        break;
      case TILED_SPLIT_TYPE_TOP:
      case TILED_SPLIT_TYPE_LEFT:
        container->container1 = TILED_CONTAINER (tiled_container_new (NULL, container));
        container->container2 = TILED_CONTAINER (tiled_container_new (element, container));
        break;
    }
  g_object_unref (element);
  gtk_paned_add1 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (container->container1)));
  gtk_paned_add2 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (container->container2)));
  gtk_container_add (GTK_CONTAINER (container), GTK_WIDGET (g_object_ref_sink (container->paned)));
  gtk_widget_show_all (GTK_WIDGET (container->paned));

  g_signal_emit (container->top_container ? container->top_container : container,
                 signals[ADDED_TILE],
                 0,
                 container->container2,
                 split_data);
}

/**
 * tiled_container_remove_child_element:
 * @container: (not nullable): a @TiledContainer
 * @child: (not nullable): a @TiledElement
 *
 * Removes the @TiledElement @child from inside the @TiledContainer @container,
 * and reorders the @TiledContainer tree.
 */

void
tiled_container_remove_child_element (TiledContainer *container,
                                      TiledElement   *child)
{
  g_return_if_fail (TILED_IS_CONTAINER (container));
  g_return_if_fail (TILED_IS_ELEMENT (child));

  if (container->element != child)
    {
      g_warning ("Trying to remove a TiledElement from a TiledContainer that doesn't contain it.");
      return;
    }
  tiled_container_element_removed_from_tree (container, child);
  tiled_container_set_tiled_element (container, NULL);
  gtk_widget_destroy (GTK_WIDGET (child));

  if (container->parent_container)
    {
      tiled_container_remove_child_container (container->parent_container, container);
    }
  else
    g_signal_emit (container, signals[EMPTY_TREE], 0);
}

/**
 * tiled_container_remove_child_container:
 * @container: (not nullable): a @TiledContainer
 * @child: (not nullable): a @TiledContainer
 *
 * Removes the @TiledContainer @child from inside the @TiledContainer @container,
 * and reorders the @TiledContainer tree.
 */

void
tiled_container_remove_child_container (TiledContainer *container,
                                        TiledContainer *child)
{
  TiledContainerContents *contents;
  TiledContainer *child_container;

  g_return_if_fail (TILED_IS_CONTAINER (container));
  g_return_if_fail (TILED_IS_CONTAINER (child));

  if ((child != container->container1) && (child != container->container2))
    {
      g_warning ("Trying to remove from a TiledContainer another TiledContainer that isn't inside it.");
      return;
    }

  contents = tiled_container_transfer_contents (child);
  g_clear_object ((child == container->container1) ? &container->container1 : &container->container2);
  gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (child));
  // if we are removing a TiledContainer, it is because...

  // a) it had a TiledElement inside, and it was removed, so now that TiledContainer is fully empty
  if ((!contents->element) && (!contents->container1) && (!contents->container2))
    {
      // so we have to take the elements in the other container...
      tiled_container_contents_free (contents);
      if (container->container1)
        {
          contents = tiled_container_transfer_contents (container->container1);
          gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container1));
          g_clear_object (&container->container1);
        }
      else
        {
          contents = tiled_container_transfer_contents (container->container2);
          gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container2));
          g_clear_object (&container->container2);
        }
      // and insert them inside this one
      if (contents->element)
        {
          // The other child had only a single element
          gtk_container_remove (GTK_CONTAINER (container), GTK_WIDGET (container->paned));
          g_clear_object (&container->paned);
          tiled_container_set_tiled_element (container, contents->element);
          tiled_element_do_grab_focus (contents->element);
        }
      else
        {
          // The other child had two childs
          gtk_paned_add1 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (contents->container1)));
          gtk_paned_add2 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (contents->container2)));
          container->container1 = g_object_ref (contents->container1);
          container->container2 = g_object_ref (contents->container2);
          tiled_container_set_parent_container (container->container1, container);
          tiled_container_set_parent_container (container->container2, container);
          gtk_orientable_set_orientation (GTK_ORIENTABLE (container->paned), contents->orientation);
          tiled_container_do_grab_focus (container->container1);
        }
      tiled_container_contents_free (contents);
    }
  // b) it had two TiledContainers inside, and one of them was removed, so add the other here
  else
    {
      child_container = (contents->container1) ? contents->container1 : contents->container2;
      if (container->container1)
        {
          container->container2 = g_object_ref (child_container);
          gtk_paned_add2 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (child_container)));
          tiled_container_set_parent_container (container->container2, container);
          tiled_container_do_grab_focus (container->container2);
        }
      else
        {
          container->container1 = g_object_ref (child_container);
          gtk_paned_add1 (GTK_PANED (container->paned), GTK_WIDGET (g_object_ref_sink (child_container)));
          tiled_container_set_parent_container (container->container1, container);
          tiled_container_do_grab_focus (container->container1);
        }
      tiled_container_contents_free (contents);
    }
}

/**
 * tiled_container_transfer_contents:
 * @container: (not nullable): a @TileContainer
 *
 * Retrieves the elements inside the specified container, to allow them to be added
 * into another container. They are also removed from the container.
 *
 * Returns: (transfer full): a @TiledContainerContets with all the elements in the container.
 */

TiledContainerContents *
tiled_container_transfer_contents (TiledContainer *container)
{
  TiledContainerContents *contents;

  g_return_val_if_fail (TILED_IS_CONTAINER (container), NULL);

  contents = g_slice_new0 (TiledContainerContents);

  contents->container1 = container->container1 ? g_object_ref (container->container1) : NULL;
  contents->container2 = container->container2 ? g_object_ref (container->container2) : NULL;
  contents->element = container->element ? g_object_ref (container->element) : NULL;
  contents->orientation = container->paned ? gtk_orientable_get_orientation (GTK_ORIENTABLE (container->paned)) : GTK_ORIENTATION_HORIZONTAL;
  if (container->element)
    tiled_container_set_tiled_element (container, NULL);

  if (container->paned)
    {
      gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container1));
      gtk_container_remove (GTK_CONTAINER (container->paned), GTK_WIDGET (container->container2));
      gtk_container_remove (GTK_CONTAINER (container), GTK_WIDGET (container->paned));
      g_clear_object (&container->container1);
      g_clear_object (&container->container2);
      g_clear_object (&container->paned);
      g_clear_object (&container->parent_container);
      g_clear_object (&container->top_container);
    }

  return contents;
}

/**
 * tiled_container_set_tiled_element:
 * @container: (not nullable): a @TiledContainer
 * @element: (transfer none): a @TiledElement
 *
 * Inserts the @TiledElement in the @TiledContainer.
 */

void
tiled_container_set_tiled_element (TiledContainer *container,
                                   TiledElement   *element)
{

  g_return_if_fail (TILED_IS_CONTAINER (container));
  g_return_if_fail ((!element) || TILED_IS_ELEMENT (element));

  inner_remove_element (container);

  if (element)
    {
      container->close_signal_id = g_signal_connect (G_OBJECT (element), "close-tile", G_CALLBACK (inner_close), container);
      container->destroy_signal_id = g_signal_connect (G_OBJECT (element), "destroy", G_CALLBACK (inner_close), container);
      tiled_element_update_parent (element, GTK_WIDGET (container));
      gtk_container_add (GTK_CONTAINER (container), GTK_WIDGET (g_object_ref_sink (element)));
      container->element = g_object_ref (element);
      gtk_widget_show_all (GTK_WIDGET (element));
    }
  g_object_notify_by_pspec (G_OBJECT (container), obj_properties[PROP_TILE_ELEMENT]);
}

/**
 * tiled_container_get_top_container:
 * @container: (not nullable): a @TiledContainer
 *
 * Retrieves the top container in the tree where @container is inserted
 *
 * Returns: (not nullable) (transfer none): a @TiledContainer
 */
TiledContainer *
tiled_container_get_top_container (TiledContainer *container)
{
  g_return_val_if_fail (TILED_IS_CONTAINER (container), NULL);

  return container->top_container ? container->top_container : container;
}

/**
 * tiled_container_set_top_container:
 * @container: (not nullable): a @TiledContainer
 * @top_container: (nullable): the @TiledContainer at the top of the tree, or NULL if this
 * container is outside a tree.
 *
 * Sets the top container in the tree where @container is inserted. If the top container is
 * NULL, @container will be set as the top.
 *
 */
void
tiled_container_set_top_container (TiledContainer *container,
                                   TiledContainer *top_container)
{
  g_return_if_fail (TILED_IS_CONTAINER (container));

  if (top_container == container->top_container) {
    return;
  }

  if (container->top_container) {
    g_object_unref (container->top_container);
  }

  container->top_container = g_object_ref (top_container);
  // propagate the new top parent
  if (container->container1) {
    tiled_container_set_top_container (container->container1, top_container);
  }
  if (container->container2) {
    tiled_container_set_top_container (container->container2, top_container);
  }
  g_object_notify_by_pspec (G_OBJECT (container), obj_properties[PROP_TOP_CONTAINER]);
}

/**
 * tiled_container_get_parent_container:
 * @container: (not nullable): a @TiledContainer
 *
 * Retrieves the @TiledContainer that contains this @TiledContainer.
 *
 * Returns: (not nullable) (transfer none): The container that contains this container,
 * or NULL if this is at the top.
 */

TiledContainer *
tiled_container_get_parent_container (TiledContainer *container)
{
  g_return_val_if_fail (TILED_IS_CONTAINER (container), NULL);

  return container->parent_container;
}

/**
 * tiled_container_set_parent_container:
 * @container: (not nullable): a @TiledContainer
 * @parent: (nullable): a @TiledContainer
 *
 * Sets @parent as the parent element of the tiled tree.
 */

void
tiled_container_set_parent_container (TiledContainer *container,
                                      TiledContainer *parent)
{

  g_return_if_fail (TILED_IS_CONTAINER (container));
  g_return_if_fail ((!parent) || TILED_IS_CONTAINER (parent));

  if (container->parent_container)
    g_clear_object (&container->parent_container);

  if (parent)
    {
      container->parent_container = g_object_ref (parent);
      tiled_container_set_top_container (container, tiled_container_get_top_container (parent));
    }
  else
    {
      container->parent_container = NULL;
      tiled_container_set_top_container (container, NULL);
    }
  g_object_notify_by_pspec (G_OBJECT (container), obj_properties[PROP_PARENT_CONTAINER]);
}

static TiledElement *
inner_get_neighbour (TiledContainer *container,
                     TiledContainer *sender,
                     TiledNeighbourDirection direction,
                     gboolean searching_up)
{
  if (!container)
    return NULL;

  if (!sender)
    sender = container->container1;

  if (container->element)
    {
      if (searching_up)
        {
          if (container->parent_container)
            return inner_get_neighbour(container->parent_container, container, direction, TRUE);
        }
      else
        {
          return container->element;
        }
    }

  switch (direction) {
  case TILED_NEIGHBOUR_DIRECTION_UP:
  case TILED_NEIGHBOUR_DIRECTION_DOWN:
    if (searching_up)
      {
        if (container->splitted_horizontal)
          {
            if ((direction == TILED_NEIGHBOUR_DIRECTION_UP) && (sender == container->container2))
              {
                return inner_get_neighbour (container->container1, container, direction, FALSE);
              }
            else
              {
                if ((direction == TILED_NEIGHBOUR_DIRECTION_DOWN) && (sender == container->container1))
                  return inner_get_neighbour (container->container2, container, direction, FALSE);
                else
                  if (container->parent_container)
                    return inner_get_neighbour (container->parent_container, container, direction, TRUE);
              }
          }
        else
          {
            if (container->parent_container)
              return inner_get_neighbour (container->parent_container, container, direction, TRUE);
          }
      }
    else
      {
        if (direction == TILED_NEIGHBOUR_DIRECTION_UP)
          return inner_get_neighbour (container->container2, container, direction, FALSE);
        else
          return inner_get_neighbour (container->container1, container, direction, FALSE);
      }
    break;

  case TILED_NEIGHBOUR_DIRECTION_LEFT:
  case TILED_NEIGHBOUR_DIRECTION_RIGHT:
    if (searching_up)
      {
        if (!container->splitted_horizontal)
          {
            if ((direction == TILED_NEIGHBOUR_DIRECTION_LEFT) && (sender == container->container2))
              {
                return inner_get_neighbour (container->container1, container, direction, FALSE);
              }
            else
              {
                if ((direction == TILED_NEIGHBOUR_DIRECTION_RIGHT) && (sender == container->container1))
                  return inner_get_neighbour (container->container2, container, direction, FALSE);
                else
                  if (container->parent_container)
                    return inner_get_neighbour (container->parent_container, container, direction, TRUE);
              }
          }
        else
          {
            if (container->parent_container)
              return inner_get_neighbour (container->parent_container, container, direction, TRUE);
          }
      }
    else
      {
        if (direction == TILED_NEIGHBOUR_DIRECTION_LEFT)
          return inner_get_neighbour (container->container2, container, direction, FALSE);
        else
          return inner_get_neighbour (container->container1, container, direction, FALSE);
      }
    break;
  }
  return NULL;
}

/**
 * tiled_container_get_neighbour:
 * @container: a @TiledContainer
 * @direction: the direction into which search for a neighbour
 *
 * This method will return the @TiledElement located in the direction specified in @direction.
 * It is useful to move the focus from one tile to any of its neighbours using, for example,
 * the cursors.
 *
 * returns: (nullable) (transfer none): the neighbour located in the specified direction, or NULL
 * if there are no neighbours.
 */
TiledElement *
tiled_container_get_neighbour (TiledContainer          *container,
                               TiledNeighbourDirection  direction)
{
  g_return_val_if_fail (TILED_IS_CONTAINER (container), NULL);
  return inner_get_neighbour (container, NULL, direction, TRUE);
}

/**
 * tiled_container_do_grab_focus:
 * @container: a @TiledContainer
 *
 * Notifies to the child element that it should grab the focus.
 */
void
tiled_container_do_grab_focus (TiledContainer *container)
{
  g_return_if_fail (TILED_IS_CONTAINER (container));

  if (container->element)
    {
      tiled_element_do_grab_focus (container->element);
    }
  else
    {
      if (container->container1)
        tiled_container_do_grab_focus (container->container1);
    }
}


/**
 * tiled_container_element_removed_from_tree:
 * @container: a @TiledContainer
 * @element: a @TiledElement
 *
 * Notifies that @element has been removed from the tree.
 */
void
tiled_container_element_removed_from_tree (TiledContainer *container,
                                           TiledElement   *element)
{
  g_return_if_fail (TILED_IS_CONTAINER (container));
  g_return_if_fail (TILED_IS_ELEMENT (element));

  if (container->top_container)
    {
      tiled_container_element_removed_from_tree (container->top_container, element);
    }
  else
    {
      if (element == container->last_focus_in_tree)
        g_clear_object (&container->last_focus_in_tree);
    }
}

/**
 * tiled_container_new:
 * @element: (nullable): the @TiledElement to insert into this container, or NULL to not set an element yet.
 * @parent_container: (nullable): the @TiledContainer where this container will be inserted.
 *
 * Creates a new #TiledContainer widget.
 *
 * Returns: the new #TiledContainer
 *
 */
GtkWidget *
tiled_container_new (TiledElement   *element,
                     TiledContainer *parent_container)
{
  TiledContainer *container = g_object_new (TILED_TYPE_CONTAINER,
                                            "tiled-element",
                                            element,
                                            "parent-container",
                                            parent_container,
                                            NULL);
  return GTK_WIDGET (container);
}
