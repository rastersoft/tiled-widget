/* -*- Mode: C; c-file-style: "gnu"; tab-width: 4 -*- */
/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/* inclusion guard */
#ifndef __TILED_CONTAINER_H__
#define __TILED_CONTAINER_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include "tiledelement.h"
#include "tileddefs.h"

G_BEGIN_DECLS

#define TILED_TYPE_CONTAINER (tiled_container_get_type())
G_DECLARE_FINAL_TYPE (TiledContainer, tiled_container, TILED, CONTAINER, GtkBin)

typedef struct
{
  TiledContainer *container1;
  TiledContainer *container2;
  TiledElement   *element;
  GtkOrientation  orientation;
} TiledContainerContents;

GType
tiled_container_contents_get_type (void);

GtkWidget *
tiled_container_new (TiledElement *element,
                     TiledContainer *parent_container);

void
tiled_container_set_tiled_element (TiledContainer *container,
                                   TiledElement *element);

TiledContainer *
tiled_container_get_top_container (TiledContainer *container);

void
tiled_container_set_top_container (TiledContainer *container,
                                   TiledContainer *top_container);

void
tiled_container_set_parent_container (TiledContainer *container,
                                      TiledContainer *parent);

TiledContainer *
tiled_container_get_parent_container (TiledContainer *container);

void
tiled_container_split (TiledContainer *container,
                       TiledSplitType  split_type,
                       guint32         split_data);

void
tiled_container_remove_child_element (TiledContainer *container,
                                      TiledElement *child);

void
tiled_container_remove_child_container (TiledContainer *container,
                                        TiledContainer *child);

TiledElement *
tiled_container_get_neighbour (TiledContainer *container,
                               TiledNeighbourDirection direction);

void
tiled_container_do_grab_focus (TiledContainer *container);


void
tiled_container_element_removed_from_tree (TiledContainer *container,
                                           TiledElement   *element);

TiledContainerContents*
tiled_container_transfer_contents (TiledContainer *container);

G_END_DECLS

#endif /* __TILED_CONTAINER_H__ */
