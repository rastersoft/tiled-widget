/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/* inclusion guard */
#ifndef __TILED_PANED_PERCENTAGE_H__
#define __TILED_PANED_PERCENTAGE_H__

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TILED_TYPE_PANED_PERCENTAGE (tiled_paned_percentage_get_type())
G_DECLARE_DERIVABLE_TYPE (TiledPanedPercentage, tiled_paned_percentage, TILED, PANED_PERCENTAGE, GtkPaned)

struct _TiledPanedPercentageClass
{
  GtkPanedClass parent_class;
};

GtkWidget * tiled_paned_percentage_new (GtkOrientation orientation);

G_END_DECLS

#endif /* __TILED_PANED_PERCENTAGE_H__ */
