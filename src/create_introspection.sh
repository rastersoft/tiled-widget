#!/bin/sh

LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH

gcc `pkg-config --cflags --libs gtk+-3.0` -fPIC -shared -o libtiled.so tiledpanedpercentage.c tiledcontainer.c tiledelement.c
g-ir-scanner --warn-all --namespace=Tiled --pkg=gtk+-3.0 --include=GObject-2.0 --include=Gtk-3.0 --library=tiled --output=tiled.gir tiledpanedpercentage.c tiledpanedpercentage.h tiledcontainer.c tiledcontainer.h tiledelement.c tiledelement.h tileddefs.h
vapigen --library=tiled --pkg=gtk+-3.0 tiled.gir
rm libtiled.so
