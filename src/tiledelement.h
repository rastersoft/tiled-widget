/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/* inclusion guard */
#ifndef __TILED_ELEMENT_H__
#define __TILED_ELEMENT_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include "tileddefs.h"

G_BEGIN_DECLS

#define TILED_TYPE_ELEMENT (tiled_element_get_type())
G_DECLARE_DERIVABLE_TYPE (TiledElement, tiled_element, TILED, ELEMENT, GtkBin)

struct _TiledElementClass
{
    GtkBinClass parent_class;
};

GtkWidget *
tiled_element_new ();

void
tiled_element_do_split_tile (TiledElement    *element,
                     TiledSplitType  type,
                     guint32         split_data);

void
tiled_element_do_close_tile (TiledElement *element);

void
tiled_element_update_parent (TiledElement *element,
                             GtkWidget    *parent);

TiledElement *
tiled_element_get_neighbour (TiledElement            *element,
                             TiledNeighbourDirection  direction);

void
tiled_element_do_grab_focus (TiledElement *element);


G_END_DECLS

#endif /* __TILED_ELEMENT_H__ */
