/*
 * Copyright (C) 2022 Sergio Costas
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/* inclusion guard */
#ifndef __TILED_DEFS_H__
#define __TILED_DEFS_H__

/**
 * TileContainerSplitType:
 * @TILE_CONTAINER_SPLIT_RIGHT: split the container vertically and add an element to the right
 * @TILE_CONTAINER_SPLIT_BOTTOM: split the container horizontally and add an element to the bottom
 */
typedef enum
{
  TILED_SPLIT_TYPE_LEFT,
  TILED_SPLIT_TYPE_RIGHT,
  TILED_SPLIT_TYPE_TOP,
  TILED_SPLIT_TYPE_BOTTOM
} TiledSplitType;

typedef enum
{
  TILED_NEIGHBOUR_DIRECTION_UP,
  TILED_NEIGHBOUR_DIRECTION_DOWN,
  TILED_NEIGHBOUR_DIRECTION_LEFT,
  TILED_NEIGHBOUR_DIRECTION_RIGHT,
} TiledNeighbourDirection;

#endif // __TILED_DEFS_H__
