CC=gcc -c
LN=gcc

COMPILE_OPTIONS=`pkg-config --cflags gtk+-3.0 vte-2.91` -O2
LINK_OPTIONS=`pkg-config --libs gtk+-3.0 vte-2.91`

DEPS=src/tiled.h src/tiledcontainer.h src/tileddefs.h src/tiledelement.h src/tiledpanedpercentage.h

all: test_terminal

test_terminal: obj/tiledcontainer.o obj/tiledelement.o obj/tiledpanedpercentage.o obj/test_terminal.o
	$(LN) obj/test_terminal.o obj/tiledcontainer.o obj/tiledelement.o obj/tiledpanedpercentage.o -o $@ $(LINK_OPTIONS)

obj/%.o: src/%.c $(DEPS)
	$(CC) $(COMPILE_OPTIONS) $< -o $@

obj/test_terminal.o: example/test_terminal.c $(DEPS)
	$(CC) $(COMPILE_OPTIONS) -Isrc example/test_terminal.c -o obj/test_terminal.o

clean:
	rm -f test_terminal obj/*.o
