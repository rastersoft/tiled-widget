# TILED WIDGET

This is a GtkWidget that simplifies the creation of tiled interfaces inside a GtkWindow. The original purpose is to simplify adding tiling mode to terminal programs like Gnome Terminal, but it is generic enough to be used in any other application.

## Usage

There is a simple example in the *example* folder, that creates a very simple terminal window with three buttons: one to split the terminal vertically, another to split it horizontally, and a third one to close that specific terminal. Also, the source code includes documentation about how to use this.

Everything revolves around the classes *TiledContainer* and *TiledElement*. *TiledElement* is derived from *GtkBox*, and is where you pack all the elements for a tile (like a terminal, a label to show the current program, a button to close the tile...). *TiledContainer* is what contains all the tiles. In fact, a *TiledContainer* can contain one *TiledElement*, or a *TiledPanedPercentage* (which is derived from *GtkPaned*) with two *TiledContainer* inside, thus creating a tree.

You have to create an initial *TiledContainer* object and connect to two signals:

* *added-tile*: this signal is emitted every time a new tile is created. The signal passes a new, recently created *TiledContainer* as a parameter, and the callback must create a new *TiledElement*, populate it with all the desired elements, and insert it into that *TiledElement* using the *tiled_container_set_tiled_element()* method.

* *empty-tree*: this signal notifies that the last tile has been closed, and thus the *TiledContainer* at the top of the tree (the one created by us) is empty and can be destroyed.

After creating the initial *TiledContainer*, a *TiledElement* must be created and set as the initial tile.

The *TiledElement* can emit the signal *request_focus*, which means that the content should grab the focus because the tile with the focus has been destroyed.

The source code contains more details.

The example also allows to move the focus between tiles using Ctrl+Shift+arrow keys.

You can generate the GObject introspection data by running the *create_introspection.sh* script.

## Gtk4 support

There is a branch called 'support_gtk4' which adds conditional compilation to support Gtk4. Unfortunately, there is a bug in Gtk4 that prevents closing tiles (<https://gitlab.gnome.org/GNOME/gtk/-/issues/4847>). A patch has been already sent (<https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/4638>). In the meantime, it implements a workaround.

## Author

Sergio Costas (raster software vigo)  
<https://www.rastersoft.com>  
<https://gitlab.com/rastersoft/tiled-widget>  
rastersoft@gmail.com
